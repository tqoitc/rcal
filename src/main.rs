#![allow(dead_code)]

extern crate rcal;

#[macro_use]
extern crate clap;

fn main() {
    let _ = clap_app!(rcal =>
                         (version: "0.0")
                         (author: "Lucas Salibian <lucas@lucassalibian.com>")
                         (about: "A simple calendar and reminder application for Linux")
                         (@arg now: -n --now "Get the current date and time")
        ).get_matches();
}
