#![allow(dead_code, non_upper_case_globals)]

#[macro_use]
extern crate lazy_static;

extern crate libc;
extern crate regex;
extern crate aho_corasick;
extern crate bincode;
extern crate time;
extern crate rustc_serialize;
extern crate flate2;

pub mod config;
pub mod calendar;

pub mod util;

lazy_static! {
    static ref CURRENT_VERSION: String = "0.0.0".to_string();
}

pub fn rcal_version() -> &'static str {
    &CURRENT_VERSION
}

pub mod serialize_helpers {
    use bincode::SizeLimit;
    use bincode::rustc_serialize::{encode as rencode, decode as rdecode};

    use rustc_serialize::{Encodable, Decodable};

    /// Can be used interchangably (almost) with
    /// `bincode::rustc_serialize::encode` however
    /// panics on error.
    /// # Example
    /// ```rust
    /// use rcal::serialize_helpers::{encode, decode};
    ///
    /// // Encode a `Vec<i32>` into `Vec<u8>`
    /// let v = vec![4i32, 5456, -23, 453, 345634, 56454];
    /// let encoded: Vec<u8> = encode(&v);
    ///
    /// // Decode the data back into the original
    /// assert_eq!(v, decode::<Vec<i32>>(&encoded));
    /// ```
    #[inline]
    pub fn encode<E: Encodable>(e: &E) -> Vec<u8> {
        rencode(e, SizeLimit::Infinite).unwrap()
    }

    /// Can be used interchangably (almost) with
    /// `bincode::rustc_serialize::encode` however
    /// panics on error.
    ///
    /// # Example
    /// ```rust
    /// use rcal::serialize_helpers::encode;
    /// let v = vec![4, 5456, 23, 453, 564];
    /// let _: Vec<u8> = encode(&v);
    /// ```
    #[inline]
    pub fn decode<D: Decodable>(d: &[u8]) -> D {
        rdecode(d).unwrap()
    }
}

#[cfg(test)]
extern crate rand;
#[cfg(test)]
mod test;
