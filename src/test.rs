use rand::{self, Rng, Rand};

use std::fmt::Debug;

use rustc_serialize::{Encodable, Decodable};
use serialize_helpers::{encode, decode};

/// Vectorized encode test
fn test_type_vec<T>()
    where T: Rand + PartialEq + Encodable + Decodable + Debug
{
    let mut rng = rand::thread_rng();
    let original = {
        let capacity = rng.gen::<usize>() % 2048;
        let mut v = Vec::with_capacity(capacity);
        for _ in 0..capacity {
            v.push(rng.gen::<T>());
        }
        v
    };
    let enc = encode(&original);
    let dec = decode::<Vec<T>>(&enc);
    assert_eq!(dec, original);
}

// Type tests

#[test]
fn test_i8_encode() {
    test_type_vec::<i8>();
}

#[test]
fn test_u8_encode() {
    test_type_vec::<u8>();
}

#[test]
fn test_i32_encode() {
    test_type_vec::<i32>();
}

#[test]
fn test_u32_encode() {
    test_type_vec::<u32>();
}

#[test]
fn test_i64_encode() {
    test_type_vec::<i64>();
}

#[test]
fn test_u64_encode() {
    test_type_vec::<u64>();
}

#[test]
fn test_f32_encode() {
    test_type_vec::<f32>();
}
#[test]
fn test_f64_encode() {
    test_type_vec::<f64>();
}

#[test]
fn test_usize_encode() {
    test_type_vec::<usize>();
}

#[test]
fn test_isize_encode() {
    test_type_vec::<isize>();
}
