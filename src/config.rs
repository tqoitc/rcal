use std::collections::HashMap;

use CURRENT_VERSION;
use util::get_home;

use std::path::Path;
use std::fs::File;

use std::fmt;
use std::io::Write;
use std::io::Error as IoError;
use std::io::ErrorKind;

use bincode::SizeLimit;
use bincode::rustc_serialize as brs;
use bincode::rustc_serialize::DecodingError;

use flate2::Compression;
use flate2::write::GzEncoder;
use flate2::read::GzDecoder;
use std::io::Read;

lazy_static! {
    static ref DEFAULT_CONFIG: Config = Config {
        version: CURRENT_VERSION.clone(),
        config: {
            let mut m = HashMap::new();
            m.insert("calendar".to_string(), Value::Str(format!("{}/.rcal", get_home())));
            m.insert("config_cache".to_string(), Value::Bool(true));
            m
        }
    };
}

#[derive(Debug, Clone, RustcEncodable, RustcDecodable)]
pub enum Value {
    Str(String),
    Float(f64),
    Int(i64),
    Bool(bool),
}

#[derive(Debug, Clone, RustcEncodable, RustcDecodable)]
pub struct Config {
    version: String,
    config: HashMap<String, Value>,
}

impl Config {
    /// Create a new config from default
    pub fn new() -> Config {
        DEFAULT_CONFIG.clone()
    }

    /// Load a cached config from the array of `u8`
    pub fn load(f: &[u8]) -> brs::DecodingResult<Config> {
        debug_assert!(f.len() > 0);
        // Create the decompressor
        let mut w = match GzDecoder::new(f) {
            Ok(v) => v,
            Err(err) => {
                let err = format!("Unable to create GzDecoder. Error: {}", err);
                // Return an error of type Decoding result with an error of type IoError the
                // internal error type of IoError::InvalidData and the extra info in `err`
                return Err(DecodingError::IoError(IoError::new(ErrorKind::InvalidData, err)));
            }
        };

        // Create the decoder and decode
        let mut dec = Vec::new();
        match w.read_to_end(&mut dec) {
            Ok(_) => brs::decode(&dec),
            Err(err) => {
                let err = format!("Unable to decode Error: {}", err);
                // Return an error of type Decoding result with an error of type IoError the
                // internal error type of IoError::InvalidData and the extra info in `err`
                return Err(DecodingError::IoError(IoError::new(ErrorKind::InvalidData, err)));
            }
        }
    }

    /// Load a cached config from file at path in `name`
    pub fn load_from_file<P: AsRef<Path>>(name: P) -> brs::DecodingResult<Config> {
        let file = match File::open(name) {
            Ok(f) => f,
            Err(err) => return Err(brs::DecodingError::IoError(err)),
        };

        // Get the decoder
        let mut decoder = match GzDecoder::new(file) {
            Ok(d) => d,
            Err(err) => {
                let err = format!("Unable to create GzDecoder. Error: {}", err);
                // Return an error of type Decoding result with an error of type IoError the
                // internal error type of IoError::InvalidData and the extra info in `err`
                return Err(DecodingError::IoError(IoError::new(ErrorKind::InvalidData, err)));
            }
        };

        brs::decode_from(&mut decoder, SizeLimit::Infinite)
    }

    /// Create a binary version of the config file.
    pub fn serialize(&self) -> Vec<u8> {
        let mut w = GzEncoder::new(Vec::new(), Compression::Default);
        brs::encode_into(self, &mut w, SizeLimit::Infinite).unwrap();
        w.finish().unwrap()
    }

    pub fn serialize_into<W: Write>(&self, w: &mut W) {
        let mut compressor = GzEncoder::new(w, Compression::Default);
        if let Err(err) = brs::encode_into(self, &mut compressor, SizeLimit::Infinite) {
            panic!("{:#?}", err);
        }
    }

    /// Add a new value to the config
    pub fn push_value(&mut self, key: &str, value: Value) {
        self.config.insert(key.to_string(), value);
    }

    /// Get a value from the config.
    pub fn get_value<T: AsRef<str>>(&self, key: T) -> Option<&Value> {
        self.config.get(&key.as_ref().to_string())
    }

    /// Check if the config has a value
    pub fn has_value<T: AsRef<str>>(&self, key: T) -> bool {
        self.config.contains_key(&key.as_ref().to_string())
    }
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        fn encode_val(v: &Value) -> String {
            match v {
                &Value::Str(ref s) => format!("\"{}\"", s),
                &Value::Float(f) => format!("{}", f),
                &Value::Int(i) => format!("{}", i),
                &Value::Bool(b) => {
                    format!("{}",
                            if b {
                                "true"
                            } else {
                                "false"
                            })
                }
            }
        }
        write!(f, "version = \"{}\";\n", self.version).unwrap();
        if f.alternate() {
            for (key, val) in &self.config {
                write!(f, "{} = {};\n", key, encode_val(val)).unwrap();
            }
        }
        Ok(())
    }
}
