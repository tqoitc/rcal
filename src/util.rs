use std::env;

use regex::Regex;

lazy_static! {
    static ref ESCAPE_NEWLINE: Regex = Regex::new("\n").unwrap();
    static ref UNESCAPE_NEWLINE: Regex = Regex::new(r"\n").unwrap();
}

pub fn escape_newlines(s: &str) -> String {
    ESCAPE_NEWLINE.replace(s, r"\n")
}

pub fn unescape_newlines(s: &str) -> String {
    UNESCAPE_NEWLINE.replace(s, "\n")
}

/// Get the home folder from the environment variable `$HOME`
///
/// # Example
/// ```rust
/// use rcal::util::get_home;
/// let home = get_home();
/// assert!(&home != "");
/// ```
pub fn get_home() -> String {
    if let Some(v) = env::vars().find(|k| &k.0 == "HOME") {
        v.1
    } else {
        "".to_string()
    }
}
