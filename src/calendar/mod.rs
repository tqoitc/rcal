use time::{Tm, Duration, now};

pub mod serializable_wrappers;

#[derive(Clone)]
pub struct Task {
    /// When should the task trigger?
    at: Tm,
    /// After how long repeat?
    repeating: Option<(Duration, i32)>,
    /// Should there be a seperate title?
    title: Option<String>,
    /// What message should be displayed
    msg: String,
}

impl Task {
    /// Create a new task with the given options.
    #[inline]
    fn new(at: Tm, repeat: Option<(Duration, i32)>, title: Option<&str>, message: &str) -> Task {
        Task {
            at: at,
            repeating: repeat,
            title: match title {
                Some(s) => Some(s.to_string()),
                None => None,
            },

            msg: message.to_string(),
        }
    }

    /// Returns the current trigger time.
    #[inline]
    fn get_trigger_time(&self) -> Tm {
        self.at
    }

    /// Get the amount of time remaining before
    /// the task is ready to trigger.
    #[inline]
    fn time_to_trigger(&self) -> Duration {
        self.at - now()
    }

    /// Check if this task repeats.
    #[inline]
    fn repeats(&self) -> bool {
        self.repeating.is_some()
    }

    /// Checks if this task has passed
    /// it's repeating time.
    #[inline]
    fn passed(&self) -> bool {
        self.time_to_trigger().num_seconds() < 0
    }

    /// Returns a tuple containing the next trigger time
    /// and the number of triggers left.
    fn peek_next_repitition(&self) -> Option<(Tm, i32)> {
        if !self.repeats() {
            return None;
        }

        let (dur, n) = self.repeating.unwrap();

        let n = if n < 0 {
            -1
        } else {
            n - 1
        };

        Some((self.at + dur, n))
    }

    /// Check if the task has used up it's assigned
    /// number of repeats.
    #[inline]
    fn expired(&self) -> bool {
        if self.repeats() {
            self.repeating.unwrap().1 == 0
        } else {
            false
        }
    }

    /// Get the next repitition while updating the
    /// remaining number of repititions. Returns the
    /// time of the next repitition and the number of
    /// repititions remaining.
    fn next_repitition(&mut self) -> Option<(Tm, i32)> {
        // Skip if we've expired or is not a repeating task
        if self.expired() || !self.repeats() {
            return None;
        }

        // Update the the new value.
        let (dur, ref mut remaining) = self.repeating.unwrap();
        *remaining -= 1;
        self.at = self.at + dur;
        let next = self.at;
        Some((next, *remaining))
    }

    /// Make the task repeat
    #[inline]
    fn set_repeat(&mut self, delay: Duration, times: i32) {
        self.repeating = Some((delay, times));
    }

    /// Remove a repeat value from this object.
    #[inline]
    fn clear_repeat(&mut self) {
        self.repeating = None;
    }

    /// Update the repeater. A value of `-1` means
    /// repeat forever.
    #[inline]
    fn set_repeat_count(&mut self, new: i32) {
        if self.repeats() {
            self.repeating.unwrap().1 = new;
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, RustcEncodable, RustcDecodable)]
pub enum Month {
    Jan,
    Feb,
    Mar,
    Apr,
    May,
    Jun,
    Jul,
    Aug,
    Sep,
    Oct,
    Nov,
    Dec,
}

impl Month {
    /// Return the month as int where
    /// January is 1 and December is 12.
    pub fn as_int(&self) -> i32 {
        match *self {
            Month::Jan => 1,
            Month::Feb => 2,
            Month::Mar => 3,
            Month::Apr => 4,
            Month::May => 5,
            Month::Jun => 6,
            Month::Jul => 7,
            Month::Aug => 8,
            Month::Sep => 9,
            Month::Oct => 10,
            Month::Nov => 11,
            Month::Dec => 12,
        }
    }
}
