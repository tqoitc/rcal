use std::ops::Deref;

use time::{self, Tm, Timespec, Duration};

use rustc_serialize::{Encodable, Decodable, Encoder, Decoder};

macro_rules! make_rules(
    ( $t:ty, $inner:ty ) => (
    impl Derived for $t {
        type Inner = $inner;
        fn inner(&self) -> &Self::Inner {
            &self.0
        }

        fn unwrap(self) -> Self::Inner {
            self.0
        }
    }

    impl Deref for $t {
        type Target = $inner;
        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }
    )
);

pub trait Derived {
    type Inner;

    /// Get a reference to the inner object
    fn inner(&self) -> &Self::Inner;

    /// Consumes the wrapper object and returns the wrapped object
    fn unwrap(self) -> Self::Inner;
}


/// A encodable/decodable wrapper around `time::Tm`
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct STm(Tm);
make_rules!(STm, Tm);

impl Encodable for STm {
    fn encode<E: Encoder>(&self, e: &mut E) -> Result<(), E::Error> {
        let timespec = self.0.to_timespec();
        try!(e.emit_i64(timespec.sec));
        try!(e.emit_i32(timespec.nsec));
        Ok(())
    }
}

impl Decodable for STm {
    fn decode<D: Decoder>(d: &mut D) -> Result<STm, D::Error> {

        let sec = try!(d.read_i64());
        let nsec = try!(d.read_i32());

        let t = Timespec {
            sec: sec,
            nsec: nsec,
        };

        Ok(STm(time::at(t)))

    }
}

/// A encodable/decodable wrapper around `time::Timespec`
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct STimespec(Timespec);
make_rules!(STimespec, Timespec);

impl Encodable for STimespec {
    fn encode<E: Encoder>(&self, e: &mut E) -> Result<(), E::Error> {
        try!(e.emit_i64(self.0.sec));
        try!(e.emit_i32(self.0.nsec));
        Ok(())
    }
}

impl Decodable for STimespec {
    fn decode<D: Decoder>(d: &mut D) -> Result<STimespec, D::Error> {
        let sec = try!(d.read_i64());
        let nsec = try!(d.read_i32());

        let t = Timespec {
            sec: sec,
            nsec: nsec,
        };

        Ok(STimespec(t))
    }
}

/// A encodable/decodable wrapper around `time::Duration`
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct SDuration(Duration);
make_rules!(SDuration, Duration);

impl Encodable for SDuration {
    fn encode<E: Encoder>(&self, e: &mut E) -> Result<(), E::Error> {
        try!(e.emit_i64(self.0.num_milliseconds()));
        Ok(())
    }
}

impl Decodable for SDuration {
    fn decode<D: Decoder>(d: &mut D) -> Result<SDuration, D::Error> {
        let dur = Duration::milliseconds(try!(d.read_i64()));
        Ok(SDuration(dur))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use time;

    use serialize_helpers::{encode, decode};

    // STm tests

    #[test]
    fn test_stm_encode() {
        let t = STm(time::now());
        encode(&t);
    }

    #[test]
    fn test_stm_decode() {
        let t = STm(time::now());
        let data = encode(&t);
        let dec = decode(&data);
        assert_eq!(t, dec);
    }

    // STimespec tests

    #[test]
    fn test_stimespec_encode() {
        let t = STimespec(time::now().to_timespec());
        encode(&t);
    }

    #[test]
    fn test_stimespec_decode() {
        let t = STimespec(time::now().to_timespec());
        let data = encode(&t);
        let dec = decode(&data);
        assert_eq!(t, dec);
    }

    // SDuration tests

    #[test]
    fn test_sduration_encode() {
        let d = SDuration(time::Duration::seconds(5));
        encode(&d);
    }

    #[test]
    fn test_sduration_decode() {
        use std::time::Duration as StdDuration;
        use std::thread::sleep;

        let d = vec![SDuration(time::Duration::seconds(5)),
                     SDuration({let n = time::now(); sleep(StdDuration::from_millis(500)); time::now() - n})];
        let data: Vec<u8> = encode(&d);
        let dec: Vec<SDuration> = decode(&data);
        assert_eq!(&dec[0], &d[0]);
        // Ensure that not too much precision was lost
        assert!(dec[1].inner().clone() - d[1].inner().clone() <= time::Duration::milliseconds(500));
    }

}
