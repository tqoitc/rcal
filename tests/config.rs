extern crate rcal;
use rcal::config::{Config, Value};

fn main() {
    let mut config = Config::new();

    let first_encode = config.serialize();
    assert_eq!(config.serialize().len(), first_encode.len());

    // Add some data
    config.push_value("somedata", Value::Str("string data".to_string()));

    assert!(first_encode.len() > config.serialize().len());

    let n = Config::load(&first_encode).unwrap();
    assert!(!n.has_value("somedata"));
}
